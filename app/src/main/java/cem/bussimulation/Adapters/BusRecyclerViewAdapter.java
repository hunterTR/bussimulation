package cem.bussimulation.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import cem.bussimulation.Models.Bus;
import cem.bussimulation.Models.BusStop;
import cem.bussimulation.Models.Route;
import cem.bussimulation.R;

/**
 * Created by cem.kaya on 15-Dec-15.
 */
public class BusRecyclerViewAdapter extends RecyclerView.Adapter<BusRecyclerViewAdapter.ViewHolder> {

    private ArrayList<Bus> BusList;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private Context context;
        public TextView name;
        public TextView line;
        public TextView remainingTime;
        // public SwipeRefreshLayout  swipeContainer;
        public ViewHolder(final Context context,View itemView) {
            super(itemView);
            this.context = context;
            name = (TextView) itemView.findViewById(R.id.name);
            line = (TextView) itemView.findViewById(R.id.line);
            remainingTime =(TextView) itemView.findViewById(R.id.remainingTime);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, name.getText(), Toast.LENGTH_SHORT).show();
                }
            });

       /*     swipeContainer = (SwipeRefreshLayout) itemView.findViewById(R.id.swipeContainer);


            swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    // Your code to refresh the list here.
                    // Make sure you call swipeContainer.setRefreshing(false)
                    // once the network request has completed successfully.
                    //  fetchTimelineAsync(0);

                    swipeContainer.setRefreshing(false);

                }
            });

            swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                    android.R.color.holo_green_light,
                    android.R.color.holo_orange_light,
                    android.R.color.holo_red_light);

*/
        }

    }

    public void swap(ArrayList<Bus> datas){
        BusList = datas;
        notifyDataSetChanged();
    }


    // Provide a suitable constructor (depends on the kind of dataset)
    public BusRecyclerViewAdapter(ArrayList<Bus> busList) {
        BusList = busList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public BusRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                  int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_bus, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(parent.getContext(),v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.name.setText(BusList.get(position).Name);
        holder.line.setText(BusList.get(position).BusRoute.Name);
        if(BusList.get(position).RemainingTimeToStop !=0 && BusList.get(position).RemainingTimeToStop !=1000 ){
            holder.remainingTime.setText(Integer.toString(BusList.get(position).RemainingTimeToStop) + " Seconds");
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return BusList.size();
    }

}
