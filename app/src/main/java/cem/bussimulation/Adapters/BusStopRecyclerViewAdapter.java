package cem.bussimulation.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import cem.bussimulation.Activities.BusStopDetailsActivity;
import cem.bussimulation.Activities.RouteDetailsActivity;
import cem.bussimulation.Models.BusStop;
import cem.bussimulation.R;

/**
 * Created by cem.kaya on 15-Dec-15.
 */
public class BusStopRecyclerViewAdapter extends RecyclerView.Adapter<BusStopRecyclerViewAdapter.ViewHolder> {

    private String[] mDataset;
    private ArrayList<BusStop> BusStopList;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private Context context;
        public TextView id;
        public TextView arrivingTime;
        public int position;
        public ViewHolder(final Context context,View itemView) {
            super(itemView);
            this.context = context;
            id = (TextView) itemView.findViewById(R.id.id);
            arrivingTime = (TextView) itemView.findViewById(R.id.arrivingTime);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, id.getText(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, BusStopDetailsActivity.class);
                    intent.putExtra("position",position);
                    intent.putExtra("name",id.getText());
                    context.startActivity(intent);
                }
            });
        }

    }
    public void swap(ArrayList<BusStop> datas){
        BusStopList = datas;
        notifyDataSetChanged();
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public BusStopRecyclerViewAdapter(ArrayList<BusStop> busStopList) {
        BusStopList = busStopList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public BusStopRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                   int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_busstop, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(parent.getContext(),v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.id.setText(BusStopList.get(position).Name);
        holder.position = position;
        if(BusStopList.get(position).BusArrivingTime != 0 && BusStopList.get(position).BusArrivingTime != 100000)
        {
            holder.arrivingTime.setText(Integer.toString(BusStopList.get(position).BusArrivingTime) + " Seconds");
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return BusStopList.size();
    }
}


