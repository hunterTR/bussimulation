package cem.bussimulation.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import cem.bussimulation.Activities.RouteDetailsActivity;
import cem.bussimulation.Models.BusStop;
import cem.bussimulation.Models.Route;
import cem.bussimulation.R;

/**
 * Created by cem.kaya on 15-Dec-15.
 */
public class RouteRecyclerViewAdapter extends RecyclerView.Adapter<RouteRecyclerViewAdapter.ViewHolder>  {

    private ArrayList<Route> RouteList;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private Context context;
        public int id =0;
        public TextView name;
        public TextView busStopCount;
       // public SwipeRefreshLayout  swipeContainer;
        public ViewHolder(final Context context,View itemView) {
            super(itemView);
            this.context = context;
            name = (TextView) itemView.findViewById(R.id.id);
            busStopCount = (TextView) itemView.findViewById(R.id.busStopCount);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Toast.makeText(context, name.getText(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(context, RouteDetailsActivity.class);
                    intent.putExtra("position",id);
                    context.startActivity(intent);
                }
            });

       /*     swipeContainer = (SwipeRefreshLayout) itemView.findViewById(R.id.swipeContainer);


            swipeContainer.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    // Your code to refresh the list here.
                    // Make sure you call swipeContainer.setRefreshing(false)
                    // once the network request has completed successfully.
                    //  fetchTimelineAsync(0);

                    swipeContainer.setRefreshing(false);

                }
            });

            swipeContainer.setColorSchemeResources(android.R.color.holo_blue_bright,
                    android.R.color.holo_green_light,
                    android.R.color.holo_orange_light,
                    android.R.color.holo_red_light);

*/
        }

    }



    // Provide a suitable constructor (depends on the kind of dataset)
    public RouteRecyclerViewAdapter(ArrayList<Route> routeList) {
        RouteList = routeList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RouteRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                                    int viewType) {
        // create a new view
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_routelist, parent, false);
        // set the view's size, margins, paddings and layout parameters

        ViewHolder vh = new ViewHolder(parent.getContext(),v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.name.setText(RouteList.get(position).Name);
        holder.id= position;
        String tmp = String.format("%d Stop", RouteList.get(position).BusStopList.size() );
        holder.busStopCount.setText(tmp);

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return RouteList.size();
    }
}
