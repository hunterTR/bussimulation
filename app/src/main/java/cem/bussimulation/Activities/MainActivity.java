package cem.bussimulation.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import cem.bussimulation.R;
import cem.bussimulation.Services.SimulationService;

public class MainActivity extends AppCompatActivity {
    TextView busStopCount;
    TextView routeCount;
    TextView busCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

       SimulationService sim = SimulationService.getInstance();
        if(sim != null)
        {
            sim.setInstanceNull();
        }
        Button startButton = (Button)findViewById(R.id.StartButton);

        busStopCount= (TextView) findViewById(R.id.BusStopCount);
        routeCount= (TextView) findViewById(R.id.RouteCount);
        busCount= (TextView) findViewById(R.id.BusCount);

        getSupportActionBar().setTitle("Bus Simulation");

        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(!busStopCount.getText().toString().isEmpty()&& busStopCount.getText() != null && !routeCount.getText().toString().isEmpty() &&  routeCount.getText() != null &&  !busCount.getText().toString().isEmpty() && busCount.getText() != null)
                {
                    int stopCount = Integer.parseInt(busStopCount.getText().toString());
                    if(stopCount >= 6)
                    {
                        SimulationService sim = SimulationService.getInstance(Integer.parseInt(busStopCount.getText().toString()),Integer.parseInt(routeCount.getText().toString()),Integer.parseInt(busCount.getText().toString()));
                        Intent intent = new Intent(getApplicationContext(), TabsActivity.class);
                        startActivity(intent);

                    }
                    else
                    {
                        Toast.makeText(getApplicationContext(), "Bus stop count has to be more than 5", Toast.LENGTH_LONG).show();
                    }

               }
                else
                {
                    SimulationService sim = SimulationService.getInstance(10,2,5);
                    Intent intent = new Intent(getApplicationContext(), TabsActivity.class);
                    startActivity(intent);
                }

            }
        });





    }
}
