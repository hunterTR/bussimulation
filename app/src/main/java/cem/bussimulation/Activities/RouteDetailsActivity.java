package cem.bussimulation.Activities;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import cem.bussimulation.Adapters.BusStopRecyclerViewAdapter;
import cem.bussimulation.Models.Bus;
import cem.bussimulation.Models.BusStop;
import cem.bussimulation.Models.Route;
import cem.bussimulation.R;
import cem.bussimulation.Services.SimulationService;

public class RouteDetailsActivity extends AppCompatActivity {
    SimulationService simService = SimulationService.getInstance();
    SwipeRefreshLayout mSwipeRefreshLayout;
    RecyclerView busStopsRecyclerView;
    BusStopRecyclerViewAdapter adapter;
    int position = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_route_details);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            position = extras.getInt("position");
        }



        getSupportActionBar().setTitle("Line " + Integer.toString(position));
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);

        ArrayList<Route> routeList = simService.RouteList;
        ArrayList<Bus> busList = simService.getBusesByRoute(simService.RouteList.get(position));


        for (int i = 0; i < simService.RouteList.get(position).BusStopList.size(); i++) {
            BusStop busStop = simService.RouteList.get(position).BusStopList.get(i);
            int tmp = 100000;
            for (int j = 0; j < busList.size(); j++) {
                if (simService.getBusArrivingTimeForBusStop(busStop, busList.get(j)) <= tmp) {
                    tmp = simService.getBusArrivingTimeForBusStop(busStop, busList.get(j));
                }
            }
            simService.RouteList.get(position).BusStopList.get(i).BusArrivingTime = tmp;
        }


        busStopsRecyclerView = (RecyclerView) findViewById(R.id.list);
// Create adapter passing in the sample user data
        adapter = new BusStopRecyclerViewAdapter(simService.RouteList.get(position).BusStopList);
// Attach the adapter to the recyclerview to populate items
        busStopsRecyclerView.setAdapter(adapter);
        // Set layout manager to position the items
        busStopsRecyclerView.setLayoutManager(new LinearLayoutManager(this));


        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                refreshItems();
            }
        });


    }

    void refreshItems() {
        // Load items
        // ...

        ArrayList<Route> routeList = simService.RouteList;
        ArrayList<Bus> busList = simService.getBusesByRoute(simService.RouteList.get(position));


        for (int i = 0; i < simService.RouteList.get(position).BusStopList.size(); i++) {
            BusStop busStop = simService.RouteList.get(position).BusStopList.get(i);
            int tmp = 100000;
            for (int j = 0; j < busList.size(); j++) {
                if (simService.getBusArrivingTimeForBusStop(busStop, busList.get(j)) <= tmp) {
                    tmp = simService.getBusArrivingTimeForBusStop(busStop, busList.get(j));
                }
            }
            simService.RouteList.get(position).BusStopList.get(i).BusArrivingTime = tmp;
        }

// Create adapter passing in the sample user data
        adapter.swap(simService.RouteList.get(position).BusStopList);
        // Load complete
        onItemsLoadComplete();
    }

    void onItemsLoadComplete() {
        // Update the adapter and notify data set changed
        // ...

        // Stop refresh animation
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first
        simService.resetBusStopArrivingTime();
    }


}
