package cem.bussimulation.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import cem.bussimulation.R;
import cem.bussimulation.Services.SimulationService;

public class SettingsActivity extends AppCompatActivity {

    TextView speed;
    Button setButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        getSupportActionBar().setTitle("Settings");
        speed = (TextView) findViewById(R.id.Speed);
        setButton = (Button) findViewById(R.id.button);

        setButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SimulationService.getInstance().setBusesSpeed(Integer.parseInt(speed.getText().toString()));
            }
        });
    }
}
