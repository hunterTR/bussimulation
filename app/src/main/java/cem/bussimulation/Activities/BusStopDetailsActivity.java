package cem.bussimulation.Activities;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;

import cem.bussimulation.Adapters.BusRecyclerViewAdapter;
import cem.bussimulation.Adapters.BusStopRecyclerViewAdapter;
import cem.bussimulation.Models.Bus;
import cem.bussimulation.Models.BusStop;
import cem.bussimulation.R;
import cem.bussimulation.Services.SimulationService;

public class BusStopDetailsActivity extends AppCompatActivity {
    SimulationService simService = SimulationService.getInstance();
    SwipeRefreshLayout mSwipeRefreshLayout;
    RecyclerView busStopsRecyclerView;
    BusRecyclerViewAdapter adapter;
    String name = null;
    int position = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bus_stop_details);

        Bundle extras = getIntent().getExtras();

        if (extras != null) {
            position = extras.getInt("position");
            name = extras.getString("name");
        }


        getSupportActionBar().setTitle(name);

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);

        BusStop busStop = simService.getBusStopByName(name);
        ArrayList<Bus> buses = new ArrayList<Bus>();

        buses = simService.getBusesByBusStop(busStop);

        for(int i = 0 ; i < buses.size() ; i++)
        {
            buses.get(i).RemainingTimeToStop = simService.getBusArrivingTimeForBusStop(busStop,buses.get(i));
        }



        busStopsRecyclerView = (RecyclerView) findViewById(R.id.list);
// Create adapter passing in the sample user data
        adapter = new BusRecyclerViewAdapter(buses);
// Attach the adapter to the recyclerview to populate items
        busStopsRecyclerView.setAdapter(adapter);
        // Set layout manager to position the items
        busStopsRecyclerView.setLayoutManager(new LinearLayoutManager(this));


        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                // Refresh items
                refreshItems();
            }
        });
    }

    void refreshItems() {
        // Load items
        // ...
        BusStop busStop = simService.getBusStopByName(name);
        ArrayList<Bus> buses = new ArrayList<Bus>();

        buses = simService.getBusesByBusStop(busStop);

        for(int i = 0 ; i < buses.size() ; i++)
        {
            buses.get(i).RemainingTimeToStop = simService.getBusArrivingTimeForBusStop(busStop,buses.get(i));
        }

        adapter.swap(buses);

        // Load complete
        onItemsLoadComplete();
    }

    void onItemsLoadComplete() {
        // Update the adapter and notify data set changed
        // ...

        // Stop refresh animation
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onPause() {
        super.onPause();  // Always call the superclass method first
        simService.restBusRemainingTimes();
    }
}
