package cem.bussimulation.Services;

import java.util.ArrayList;

import cem.bussimulation.Models.Bus;
import cem.bussimulation.Models.Route;
import cem.bussimulation.Utilities.Utilities;

/**
 * Created by cem on 14/12/15.
 */
public class BusService {

    private ArrayList<Bus> BusList;
    private ArrayList<Route> RouteList;
    private int BusCount;



    public BusService(int busCount,ArrayList<Route> routes) {
        BusList = new ArrayList<Bus>();
        RouteList = routes;
        BusCount = busCount;
    }


    public ArrayList<Bus> CreateBuses(){

        for(int i = 0 ; i < BusCount ; i++){
            int random = Utilities.randomNumber(0,RouteList.size());
            BusList.add(new Bus("Bus " + i,RouteList.get(random)));
        }



        return BusList;
    }



}
