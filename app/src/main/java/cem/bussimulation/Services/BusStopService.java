package cem.bussimulation.Services;

import java.util.ArrayList;

import cem.bussimulation.Models.BusStop;
import cem.bussimulation.Utilities.Utilities;

/**
 * Created by cem on 14/12/15.
 */
public class BusStopService {
    private ArrayList<BusStop> BusStopList;
    private int BusStopCount;
    private int maxMapLength;


    public BusStopService(int busStopCount){
        BusStopList = new ArrayList<BusStop>();
        BusStopCount = busStopCount;

        maxMapLength = 100*busStopCount;
    }

    public ArrayList<BusStop> CreateBusStops(){

        for(int i = 0 ; i < BusStopCount ; i++){

            BusStopList.add(new BusStop(Utilities.randomNumber(0,maxMapLength),"Stop "+i));

        }
        return BusStopList;
    }

}
