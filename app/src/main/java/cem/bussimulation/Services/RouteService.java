package cem.bussimulation.Services;

import java.util.ArrayList;
import java.util.Random;

import cem.bussimulation.Models.BusStop;
import cem.bussimulation.Models.Route;
import cem.bussimulation.Utilities.Utilities;

/**
 * Created by cem on 14/12/15.
 */
public class RouteService {
    private ArrayList<BusStop> BusStopList;
    private ArrayList<Route> RouteList;
    private int RouteCount;
    private int RandomStopCount;

    public RouteService(int routeCount, ArrayList<BusStop> busStopList) {
        RouteCount = routeCount;
        RouteList = new ArrayList<Route>();
        BusStopList = busStopList;
    }

    public ArrayList<Route> CreateRoutes() {

        for (int i = 0; i < RouteCount; i++) {
            RandomStopCount = Utilities.randomNumber(5, BusStopList.size());

            ArrayList<BusStop> tmpBusStopList = new ArrayList<BusStop>();
            for (int h = 0; h < RandomStopCount; h++) {
                while (true) {
                    int random = Utilities.randomNumber(0, BusStopList.size());
                    if (!CheckStopExistInArray(BusStopList.get(random), tmpBusStopList)) {
                        tmpBusStopList.add(BusStopList.get(random));
                        break;
                    }
                }

            }

            tmpBusStopList = getOrderedBusStopList(tmpBusStopList);
            RouteList.add(new Route("Line " + i, tmpBusStopList,getTotalLength(tmpBusStopList)));


        }
        //

        return RouteList;
    }


    private boolean CheckStopExistInArray(BusStop busStop, ArrayList<BusStop> busStopList) {

        for (int h = 0; h < busStopList.size(); h++) {
            if (busStopList.get(h).Name == busStop.Name) {
                return true;
            }
        }

        return false;
    }


    private ArrayList<BusStop> getOrderedBusStopList(ArrayList<BusStop> busStopList) {

        for (int h = 0; h < busStopList.size(); h++) {
            for (int i = 0; i < busStopList.size() - 1; i++) {
                if (busStopList.get(i).Loc > busStopList.get(i + 1).Loc) {
                    BusStop tmpBusStop = busStopList.get(i);
                    busStopList.set(i, busStopList.get(i + 1));
                    busStopList.set(i + 1, tmpBusStop);
                }

            }
        }
        return busStopList;
    }

    public int getTotalLength(ArrayList<BusStop> busList) {

        int totalLength = 0;
        for (int i = 0; i < busList.size(); i++) {
            totalLength = totalLength + (busList.get(i).Loc - busList.get(0).Loc);
        }
        return totalLength;
    }
}
