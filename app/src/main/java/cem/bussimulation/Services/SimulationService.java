package cem.bussimulation.Services;

import android.util.Log;

import java.util.ArrayList;

import cem.bussimulation.Models.Bus;
import cem.bussimulation.Models.BusStop;
import cem.bussimulation.Models.Route;

/**
 * Created by cem on 13/12/15.
 */

public class SimulationService {

    public static SimulationService instance;

    private BusStopService busStopService;
    private RouteService routeService;
    private BusService busService;
    public ArrayList<BusStop> BusStopList;
    public ArrayList<Route> RouteList;
    public ArrayList<Bus> BusList;


    public void setInstanceNull(){
        instance = null;
    }
    private SimulationService(int busStopCount, int routeCount , int busCount){
        if( busStopCount == 0)
        {
           busStopCount= 10;
        }

        if(routeCount == 0)
        {
            routeCount = 1;
        }

        if(busCount == 0)
        {
            busCount = 2;
        }
        busStopService = new BusStopService(busStopCount);
        BusStopList = busStopService.CreateBusStops();

        routeService = new RouteService(routeCount,BusStopList);
        RouteList = routeService.CreateRoutes();

        busService = new BusService(busCount,RouteList);
        BusList = busService.CreateBuses();

        StartSimulation();
    }

    public static synchronized SimulationService getInstance(int busStopCount,int routeCount,int busCount){
        if(instance== null)
        {
            instance = new SimulationService(busStopCount,routeCount,busCount);
        }
        return instance;
    }

    public static synchronized SimulationService getInstance(){
        if(instance== null)
        {
            instance = new SimulationService(0,0,0);
        }
        return instance;
    }

    public int getBusArrivingTimeForBusStop(BusStop stop,Bus bus){
        int remainingLength;
        int arrivingTime;
        if(stop.Loc >= bus.CurrentLocation)
        {
            remainingLength = stop.Loc - bus.CurrentLocation;
        }
        else
        {
            remainingLength = (bus.BusRoute.TotalLength -bus.CurrentLocation) + (stop.Loc - bus.BusRoute.BusStopList.get(0).Loc);
        }

        return remainingLength/bus.Speed;
    }

    public BusStop getBusStopByName(String name){

        for(int i = 0 ; i < BusStopList.size() ; i ++)
        {
            if(BusStopList.get(i).Name.equals(name.toString()))
            {
                return BusStopList.get(i);
            }
        }

        return null;
    }

    public ArrayList<Bus> getBusesByBusStop(BusStop stop){
        ArrayList<Bus> buses = new ArrayList<Bus>();

        for(int i = 0 ; i < BusList.size() ; i ++)
        {
           ArrayList<BusStop> BusStopListOfRoute = BusList.get(i).BusRoute.BusStopList;
            for(int j =0 ; j < BusStopListOfRoute.size();j++)
            {
                if(BusStopListOfRoute.get(j).Name == stop.Name)
                {
                    buses.add(BusList.get(i));
                }
            }
        }
        return buses;
    }


    public ArrayList<Bus> getBusesByRoute(Route route){
        ArrayList<Bus> buses = new ArrayList<Bus>();

        for(int i = 0 ; i < BusList.size() ; i ++)
        {
            if(BusList.get(i).BusRoute.Name == route.Name)
            {
                buses.add(BusList.get(i));
            }
        }
        return buses;
    }


    public void resetBusStopArrivingTime(){
        for (BusStop busStop :
                BusStopList) {
            busStop.BusArrivingTime = 0;
        }
    }

    public void restBusRemainingTimes(){
        for (Bus bus :
                BusList) {
            bus.RemainingTimeToStop = 0;
        }
    }



    public void StartSimulation(){
        new Thread(new Runnable() {
            public void run() {
                for (Bus bus :
                        BusList) {
                    bus.CurrentLocation += bus.Speed;
                    if(bus.CurrentLocation >= bus.BusRoute.TotalLength + bus.BusRoute.BusStopList.get(0).Loc)   // go to start if bus has arrived end of the route.
                    {
                        bus.CurrentLocation = 0 ;
                        Log.w("BUS STOP : ",bus.Name +" Bus has arrived to end of the line");
                    }
                }
            }
        }).start();
    }

    public void moveBuses(){
        for (Bus bus :
                BusList) {
            bus.CurrentLocation += bus.Speed;
            if(bus.CurrentLocation >= bus.BusRoute.TotalLength + bus.BusRoute.BusStopList.get(0).Loc)   // go to start if bus has arrived end of the route.
            {
                bus.CurrentLocation = 0 ;
                Log.w("BUS STOP : ",bus.Name +" Bus has arrived to end of the line");
            }
        }
    }

    public void setBusesSpeed(int speed)
    {
        for(Bus bus: BusList)
        {
            bus.Speed = speed;
        }
    }





}
