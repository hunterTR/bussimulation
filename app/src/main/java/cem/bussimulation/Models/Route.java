package cem.bussimulation.Models;

import java.util.ArrayList;

/**
 * Created by cem on 13/12/15.
 */
public class Route {
    public String Name;
    public ArrayList<BusStop> BusStopList;
    public ArrayList<BusStop> OrderedBusStopList;
    public int TotalLength;

    public Route(String name, ArrayList<BusStop> busStopList, int totalLength) {
        Name = name;
        if (busStopList == null) {
            busStopList = new ArrayList<BusStop>();
        }
        BusStopList = busStopList;
        TotalLength = totalLength;
    }

    public void AddBusStop(BusStop busStop) {
        BusStopList.add(busStop);
    }

    public void RemoveBusStop(int index) {
        BusStopList.remove(index);
    }


}
