package cem.bussimulation.Models;

import cem.bussimulation.Utilities.Utilities;

/**
 * Created by cem on 13/12/15.
 */
public class Bus {
    public Route BusRoute;
    public String Name;
    public int CurrentLocation;
    public int Speed;
    public int Direction;
    public int RemainingTimeToStop;

    public Bus(String name , Route busRoute)
    {
        Name=name;
        Direction = 1;
        BusRoute = busRoute;
        Speed=2;
        int random = Utilities.randomNumber(0,busRoute.BusStopList.size());
        CurrentLocation=busRoute.BusStopList.get(random).Loc;
        RemainingTimeToStop =0;
    }
}
