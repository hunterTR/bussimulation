package cem.bussimulation.Models;

/**
 * Created by cem on 13/12/15.
 */
public class BusStop {
    public int Loc;
    public String Name;
    public int BusArrivingTime;

    public int LengthToNextStation;

    public BusStop(int loc, String name)
    {
        Loc = loc;
        Name = name;
        LengthToNextStation = 0 ;
        BusArrivingTime = 0;
    }

    public BusStop(int loc, String name, int lengthToNextStation)
    {
        Loc = loc;
        Name = name;
        LengthToNextStation = lengthToNextStation;

    }
}
